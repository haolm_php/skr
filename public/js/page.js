$(function () {

    adjustSize();

    leftToRightNavbar();

    $('.content-wrapper').hide().fadeIn(1500);
    $('footer').hide().fadeIn(1500);

    /* For stikcy header */
    stikcyHeader();

    backToTop();
    backToTopMobile();
    iconSide();
    video();
});

function stikcyHeader() {

    var $win = $(window),
        $main = $('main'),
        $nav = $('nav'),
        $header = $('row-header'),
        navHeight = $nav.outerHeight(),
        footerHeight = $('footer').outerHeight(),
        docmentHeight = $(document).height(),
        navPos = $nav.offset().top,
        fixedClass = 'is-fixed',
        hideClass = 'is-hide';


    $win.on('load scroll', function() {
        var value = $(this).scrollTop(),
            scrollPos = $win.height() + value;

        if ( value > navPos ) {
            if ( docmentHeight - scrollPos <= footerHeight ) {
                $nav.addClass(hideClass);
                $header.addClass(hideClass);
            } else {
                $nav.removeClass(hideClass);
                $header.removeClass(hideClass);

            }
            $nav.addClass(fixedClass);
            $header.addClass(fixedClass);
            $main.css('margin-top', navHeight);
            $('.fix-logo').show();

        } else {
            $main.css('display', '0');
            $nav.removeClass(fixedClass);
            $header.removeClass(fixedClass);
            $main.css('margin-top', '0');
            $('.fix-logo').hide();
        }
    });
}

/**
 * Closing an open collapsed navbar when clicking outside
 */
function leftToRightNavbar() {

    $('[data-toggle="slide-collapse"]').on('click', function() {

        $navMenuCont = $($(this).data('target'));

        $navMenuCont.animate({
            width: [ "toggle", "swing" ],
            height: [ "toggle", "swing" ],
            opacity: "toggle"
        }, 350);

        $(".menu-overlay").fadeIn(500);

    });

    $(".menu-overlay").click(function(event) {
        $("button.navbar-toggler").trigger("click");
        $(".menu-overlay").fadeOut(500);
    });
}

function adjustSize() {

    let headerHeight = $("#header-wrapper").outerHeight();

    $("#main-content-wrapper").css({
        /*paddingTop: (headerHeight -1) + "px",*/
        minHeight: getMainContentMinHeight() + "px"
    });

    // Scroll to top when scroll-to-top arrow is clicked
    $('#back_to_top').click(function() {
        $('body, html').animate({
            scrollTop : 0
        }, 500);
    });
}

function openPage(url) {
    window.open(url, "_blank");
}

function goToPage(url) {
    location.href = url;
}

function getMainContentMinHeight() {

    let screenHeight = $(window).innerHeight();
    let headerHeight = $("#header-wrapper").outerHeight();
    let footerHeight = $("#footer-wrapper").outerHeight();
    let paddingBottom = 10;

    let mainContentMinHeight = screenHeight - headerHeight - footerHeight - paddingBottom;

    return mainContentMinHeight;
}

function backToTop() {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();

        } else {
            $('#back-to-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('#back-to-top').click(function () {

        $('body,html').animate({
            scrollTop: 0
        }, 800);

        return false;
    });

    // Ẩn hiện nội dung bài viết
    $('.read-add').hide();
    $(".btn-see").click(function () {
        $('.read-add').slideDown();
        $(".btn-compact").css("display", "block");
        $(".btn-see").hide();

    });

    $(".btn-compact").click(function () {
        $('.read-add ').slideUp();
        $(".btn-compact").hide();
        $(".btn-see").show();
    });

    // Ẩn hiện nội dung bài viết
    $('.read-service1').hide();
    $(".btn-see-sv1").click(function () {
        $('.read-service1').slideDown();
        $(".btn-compact-sv1").css("display", "block");
        $(".btn-see-sv1").hide();

    });

    $(".btn-compact-sv1").click(function () {
        $('.read-service1 ').slideUp();
        $(".btn-compact-sv1").hide();
        $(".btn-see-sv1").show();
    });

    $('.read-service2').hide();
    $(".btn-see-sv2").click(function () {
        $('.read-service2').slideDown();
        $(".btn-compact-sv2").css("display", "block");
        $(".btn-see-sv2").hide();

    });

    $(".btn-compact-sv2").click(function () {
        $('.read-service2 ').slideUp();
        $(".btn-compact-sv2").hide();
        $(".btn-see-sv2").show();
    });

}

function iconSide() {
    $('#icon-side').hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 20) {
            $('#back-to-top').fadeIn();

        } else {
            $('#back-to-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('#back-to-top').click(function () {

        $('body,html').animate({
            scrollTop: 0
        }, 800);

        return false;
    });

    // Ẩn hiện nội dung bài viết
    $('.read-add').hide();
    $(".btn-see").click(function () {
        $('.read-add').slideDown();
        $(".btn-compact").css("display", "block");
        $(".btn-see").hide();

    });

    $(".btn-compact").click(function () {
        $('.read-add ').slideUp();
        $(".btn-compact").hide();
        $(".btn-see").show();
    });

    // Ẩn hiện nội dung bài viết
    $('.read-service1').hide();
    $(".btn-see-sv1").click(function () {
        $('.read-service1').slideDown();
        $(".btn-compact-sv1").css("display", "block");
        $(".btn-see-sv1").hide();

    });

    $(".btn-compact-sv1").click(function () {
        $('.read-service1 ').slideUp();
        $(".btn-compact-sv1").hide();
        $(".btn-see-sv1").show();
    });

    $('.read-service2').hide();
    $(".btn-see-sv2").click(function () {
        $('.read-service2').slideDown();
        $(".btn-compact-sv2").css("display", "block");
        $(".btn-see-sv2").hide();

    });

    $(".btn-compact-sv2").click(function () {
        $('.read-service2 ').slideUp();
        $(".btn-compact-sv2").hide();
        $(".btn-see-sv2").show();
    });

}
function backToTopMobile() {


    // scroll body to 0px on click
    $('#top-mobile').click(function () {

        $('body,html').animate({
            scrollTop: 0
        }, 800);

        return false;
    });


    $(".btn-see-about").click(function () {
        $('.about-mobile').slideDown();
        $(".btn-compact-about").css("display", "block");
        $(".btn-see-about").hide();
    });
    $(".btn-compact-about").click(function () {
        $('.about-mobile ').slideUp();
        $(".btn-compact-about").hide();
        $(".btn-see-about").show();
    });


}


// $(".select ul li.option").click(function() {
//     $(this).siblings().addBack().children();
//     $(this).siblings().toggle();
//     $(this).parent().prepend(this);
// });

// menu active
$('#header-wrapper a[href*="#"]').on('click', function() {
    $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top + -100
    }, 300, 'linear');
    $(this).parent().addClass('active').siblings().removeClass('active');
    if ($(window).width()< 767){
        $('.mobile-nav-bar, .menu-overlay').toggle(300);
    }
});

// services
$('.slider-service').owlCarousel({
    loop:false,
    margin:0,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
});
$('.slider-logo').owlCarousel({
    loop:true,
    autoplay: true,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:2,
            nav:false,
            dots:true,
            margin:15,
        },
        600:{
            items:3,
            margin:30,
        },
        1000:{
            items:5,
            margin:30,
        }
    }
});

$('.slider-news').owlCarousel({
    loop:true,
    margin:30,
    dots:true,
    responsive:{
        0:{
            items:1
        },
        1000:{
            items:1
        }
    }
});
$('.slider-news-1').owlCarousel({
    loop:false,
    margin:10,
    dots:false,
    nav: true,
    Height:210,
    navText: [$('.am-next'),$('.am-prev')],
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
$('.slider-main').owlCarousel({
    loop:true,
    items:1,
    dots:false,
    nav: true,
    autoplay:true,
    autoplayTimeout: 2000,
    navText: [$('.next'),$('.prev')],
});

function video(){
    // if the cookie doesn't exist create it and show the modal
    if ( ! $.cookie('hereToday') ) {
        var date = new Date();
        date.setTime(date.getTime() + ( 1800 * 1000));
        // create the cookie. Set it to expire in 30m
        $.cookie('hereToday', true, { expires: date });
        //call video modal
        var delay=5000; //in ms, this would mean 5 seconds
        setTimeout(function(){
            $('#modalYT').modal('toggle');

        },delay);
    }
    // set time logo video show
    setTimeout(function(){
        $('#icon-side').fadeIn();
    },6000);
}

$("li.customer-vn").click(function() {
    $('#customer-jp').addClass('active');
    $('#customer-vn').removeClass('active');
    $(this).css('display', 'none');
    $(' #customer-jp .owl-nav').css('display', 'block');
    $('li.customer-jp').css('display', 'block');
});

$("li.customer-jp").click(function() {
    $('#customer-vn').addClass('active');
    $('#customer-jp').removeClass('active');
    $(this).css('display', 'none');
    $('li.customer-vn').css('display', 'block');
    $(' #customer-jp .owl-nav').css({"display":"none","transition":".5s"});
});

$('#top_navbar li.languages').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});