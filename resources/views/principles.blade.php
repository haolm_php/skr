@extends('layouts.app')
@section('content')
    @include('layouts.header_page')
    <section id="header-home">
            <div class="top-services content">
                <ul class="b-crumbs">
                    <li>
                        <a href="{{url('/')}}">
                            {{__('home.home')}}
                        </a>
                    </li>
                    <li>
                        <a href="">
                            {{__('home.guiding')}}
                        </a>
                    </li>
                </ul>
            </div>
    </section>
    <section id="content-principles">
        <div class="contain">
            <div class="home-principles">
                <div class="text-principles colabout">
                    <div class="mb-3 ">
                        <h2>{{__('home.guiding')}}</h2>
                    </div>
                </div>
            </div>
            <div class="row-principles">
                <div class="row">
                    <div class=" col-lg-7 col-md-12 col-sm-12">
                        <div class="principles-image">
                            <img src="{{asset('img/sakuraimage/GuildingPriciple1.png')}}">
                        </div> 
                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12">
                        <div class="colprinciples-1 ">
                           <p>{{__('home.guiding-r1')}}.</p>
                            <p>{{__('home.guiding-r2')}}.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-guiding">
                <div class="content-guiding">
                    <h4>{{__('home.why-choose')}}</h4>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="guiding-principles">
                            <h1>{{__('home.guiding-management')}}</h1>
                            <h1>{{__('home.guiding-create')}}</h1>
                        </div> 
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="guiding-principles-1 ">
                           <div class="content-principles contain-guiding">
                                <h4>{{__('home.guiding-t1')}}</h4>
                                <p>{{__('home.guiding-c1')}}</p>
                           </div>
                           <div class="content-principles">
                                <h4>{{__('home.guiding-t2')}}</h4>
                                <p>{{__('home.guiding-c2')}}</p>
                           </div>
                           <div class="content-principles contain-principle">
                                <h4>{{__('home.guiding-t3')}}</h4>
                                <p>{{__('home.guiding-c3')}}</p>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.contact')
@endsection
