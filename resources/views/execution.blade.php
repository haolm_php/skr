@extends('layouts.app')
@section('content')
    @include('layouts.header_page')
    <section id="header-home">
        <div class="content">
            <div class="top-services">
                <ul class="b-crumbs">
                    <li>
                        <a href="{{url('/')}}">
                            {{__('home.home')}}
                        </a>
                    </li>
                    <li>
                        <a href="catalog-list.html">
                            {{__('home.execution')}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section id="content-Execution">
        <div class="container">
            <div class="home-Execution">
                <div class="text-principles colabout">
                    <div class="mb-3 ">
                        <h2>{{__('home.execution')}}</h2>
                    </div>
                </div>
                <div class="contain-Execution">
                    <div class="text-Execution-1">
                        <p>{{__('home.execution-h')}}</p>
                    </div>
                </div>
                <div class="text-Execution-2">
                    <div class="row-execution">
                        <div class="text-Execution-big">
                            <div class="Execution-child-img">
                                <img class="show" src="{{asset('/img/icon/listening.png')}}" alt="">
                                <img class="hidden" src="{{asset('/img/icon/listeningh.png')}}" alt="">
                            </div>
                            <div class="Execution-child-2">
                                <p class="page-child">1. {{__('home.execution-t1')}}</p>
                            </div>
                            <div class="Execution-child-1">
                                <p>{{__('home.execution-c1')}}</p>
                            </div>
                        </div>
                        <div class="text-Execution-big">
                            <div class="Execution-child-img">
                                <img class="show" src="{{asset('/img/icon/organize.png')}}" alt="">
                                <img class="hidden" src="{{asset('/img/icon/organizeh.png')}}" alt="">
                            </div>
                            <div class="Execution-child-2">
                                <p class="page-child">2. {{__('home.execution-t2')}}</p>
                            </div>
                            <div class="Execution-child-1">
                                <p>{{__('home.execution-c2')}}</p>
                            </div>
                        </div>
                        <div class="text-Execution-big">
                            <div class="Execution-child-img">
                                <img class="show" src="{{asset('/img/icon/demonstration.png')}}" alt="">
                                <img class="hidden" src="{{asset('/img/icon/demonstrationh.png')}}" alt="">
                            </div>
                            <div class="Execution-child-2">
                                <p class="page-child">3. {{__('home.execution-t3')}}</p>
                            </div>
                            <div class="Execution-child-1">
                                <p>{{__('home.execution-c3')}}</p>
                            </div>
                        </div>
                        <div class="text-Execution-big">
                            <div class="Execution-child-img">
                                <img class="show" src="{{asset('/img/icon/time.png')}}" alt="">
                                <img class="hidden" src="{{asset('/img/icon/timeh.png')}}" alt="">
                            </div>
                            <div class="Execution-child-2">
                                <p class="page-child">4. {{__('home.execution-t4')}}</p>
                            </div>
                            <div class="Execution-child-1">
                                <p>{{__('home.execution-c4')}}</p>
                            </div>
                        </div>
                        <div class="text-Execution-big">
                            <div class="Execution-child-img">
                                <img class="show" src="{{asset('/img/icon/contract.png')}}" alt="">
                                <img class="hidden" src="{{asset('/img/icon/contracth.png')}}" alt="">
                            </div>
                            <div class="Execution-child-2">
                                <p class="page-child">5. {{__('home.execution-t5')}}</p>
                            </div>

                            <div class="Execution-child-1">
                                <p>{{__('home.execution-c5')}}</p>
                            </div>
                        </div>
                        <div class="text-Execution-big">
                            <div class="Execution-child-img">
                                <img class="show" src="{{asset('/img/icon/accept.png')}}" alt="">
                                <img class="hidden" src="{{asset('/img/icon/accepth.png')}}" alt="">
                            </div>
                            <div class="Execution-child-2">
                                <p class="page-child">6. {{__('home.execution-t6')}}</p>
                            </div>
                            <div class="Execution-child-1">
                                <p>{{__('home.execution-c6')}}</p>
                            </div>
                        </div>
                        <div class="text-Execution-big">
                            <div class="Execution-child-img">
                                <img class="show" src="{{asset('/img/icon/phonebook.png')}}" alt="">
                                <img class="hidden" src="{{asset('/img/icon/phonebookh.png')}}" alt="">
                            </div>
                            <div class="Execution-child-2">
                                <p class="page-child">7. {{__('home.execution-t7')}}</p>
                            </div>
                            <div class="Execution-child-1">
                                <p>{{__('home.execution-c7')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="line-home">
        </div>
    </section>
    @include('layouts.contact')
@endsection
