@extends('layouts.app')
@section('content')
    @include('layouts.header_page')
    <section id="header-home container">
        <div class="top-services content">
            <div class="row  news-page">
                <div class="col-md-8 col-12 new-content">
                    @if (session('lang')=='vi' )
                        <h3>{{$news['tittle_vn']}}</h3>
                        <div>{!! $news['content_vn'] !!} </div>
                    @elseif (session('lang')=='jp' )
                        <h3>{{$news['tittle_jp']}}</h3>
                        <div>{!! $news['content_jp'] !!} </div>
                    @else
                        <h3>{{$news['tittle_en']}}</h3>
                        <div>{!! $news['content_en'] !!} </div>
                    @endif
                </div>
                <div class="col-md-4 col-12">
                    <div class="block_widget">
                        <div class="title_widget width_common_widget">
                            <div class="title_widget_left">
                                <a href="" class="title_txt pt_thegioi" target="_blank">Tin Hot</a>
                            </div>
                            <div class="title_widget_right">
                                <a href="http://sakuraecology.com" class="txt_name_portal" target="_blank">sakuraecology.com</a>
                            </div>
                        </div>
                        <ul class="list_news_widget_1">
                            @foreach($news_hot as $row)
                                    <li class="">
                                        <a href="{{url('/news')}}/{{$row->slug}}"
                                           class="wg_image_thumb" target="_blank"
                                           title="{{$row->tittle_en}}">
                                            <img src="{{asset('img/upload/')}}/{{$row->avatar}}"
                                                 class="vne_lazy_image"
                                                 alt="{{$row->tittle_en}}">
                                        </a>
                                        @if (session('lang')=='vi' )
                                            <a href="{{$row->slug}}"
                                               class="txt_wg_title" target="_blank"
                                               title="{{$row->tittle_vn}}">{{$row->tittle_vn}}</a>
                                        @elseif (session('lang')=='jp' )
                                            <a href="{{url('/news')}}/{{$row->slug}}"
                                               class="txt_wg_title" target="_blank"
                                               title="{{$row->tittle_vn}}">{{$row->tittle_jp}}</a>
                                        @else
                                            <a href="{{url('/news')}}/{{$row->slug}}"
                                               class="txt_wg_title" target="_blank"
                                               title="{{$row->tittle_vn}}">{{$row->tittle_en}}</a>
                                        @endif
                            @endforeach
                        </ul>
                    </div>

                </div>

            </div>
            <div id="comment">
                <div class="fb-comments fb_iframe_widget" style="width: 100% ; margin-bottom: 20px; " >
                    <div class="fb-comments"
                         data-href="{{url('details')}}/{{$news['id']}}"
                         data-width="100%" data-numposts="5"></div>
                </div>
            </div>
        </div>
    </section>

@endsection
