<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'SKR') }} | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#007bff"/>
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#007bff">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#007bff">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/fontawesome.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('lib/adminlte/css/adminlte.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <style>
        body {
            width: 450px;
            height: 100%;
            z-index: -9999;
            margin-left: auto;
            margin-right: auto;
            margin-top: 80px;
            margin-bottom: auto;
            overflow: hidden;
        }

        canvas {
            background-color: #e9ecef;
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            z-index: -9999;

        }
    </style>
</head>

<body>
@include('sweetalert::alert')
<!-- background animate -->
<canvas id="dot-connect"></canvas>
<!-- /.background animate -->


<!-- Login -->
<div class="login-box pass-word" style="width: 450px">
    <div class="login-logo ">
        <a href="#"><b class="text-primary font-weight-bold">SAKURA</b>-ADMIN</a>
    </div>
    <!-- /.login-logo -->
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="card card-primary card-outline">
        <div class="forgot-pass">
            Reset your password
        </div>
        <div class="card-body login-card-body">
            <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
            @csrf
            <!--Email-->
                <div class="row">
                    <label for="email" class=" col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                    <div class="input-group ">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-envelope " style="font-size: 14px"></i></span>
                            </div>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="password" class=" col-form-label text-md-right">{{ __('Password') }}</label>
                    <div class="input-group ">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-lock "></i></span>
                            </div>
                            <input id="password" type="password" value="{{old('password')}}" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="input-group">
                        <label for="password-confirm" class=" col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-check-square " style="font-size: 15px"></i></span>
                            </div>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <button type="submit"
                            class="btn btn-primary btn-block btn-flat"> Update</button>
                </div>
                <div class="form-check-label back-lg" for="remember" style="font-size: 14px;padding: 18px">
                    <a href="{{url('/login')}}">
                        Back to login page
                    </a>
                </div>

            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('lib/iCheck/icheck.min.js')}}"></script>
<!-- dot-connect -->
<script src="{{asset('lib/canvas-animation/dot-connect.min.js')}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        })
    })
</script>
</body>
</html>