@extends('layouts.app')
@section('content')
    @include('layouts.header_page')
    <section id="header-home ">
        <div class="content">
            <div class="top-services">
                <ul class="b-crumbs">
                    <li>
                        <a href="{{url('/')}}" ref="">
                            {{__('home.home')}}
                        </a>
                    </li>
                    <li>
                        <a href="" ref="">
                            {{__('home.business')}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section id="content-bussiness">
        <div class="container">
            <div class="home-bussiness">
                <div class="text-principles colabout">
                    <div class="mb-3 ">
                        <h2>{{__('home.business')}}</h2>
                    </div>
                    <div class="text-bussiness-1">
                        <p>{{__('home.business-page-t')}}</p>
                    </div>
                    <div class="text-row-bussiness">
                        <ul class="row">
                            <li class="col">
                                <div class="col-bussiness">
                                    <div class="icon-bussiness">
                                        <p>1</p>
                                    </div>
                                    <h4>{{__('home.business-1')}}</h4>
                                </div>
                            </li>
                            <li class="col">
                                <div class="col-bussiness">
                                    <div class="icon-bussiness">
                                        <p>2</p>
                                    </div>
                                    <h4>{{__('home.business-2')}}</h4>
                                </div>
                            </li>
                            <li class="col">
                                <div class="col-bussiness">
                                    <div class="icon-bussiness">
                                        <p>3</p>
                                    </div>
                                    <h4>{{__('home.business-3')}}</h4>
                                </div>
                            </li>
                            <li class="col">
                                <div class="col-bussiness">
                                    <div class="icon-bussiness">
                                        <p>4</p>
                                    </div>
                                    <h4>{{__('home.business-4')}}</h4>
                                </div>
                            </li>
                            <li class="col">
                                <div class="col-bussiness">
                                    <div class="icon-bussiness">
                                        <p>5</p>
                                    </div>
                                    <h4>{{__('home.business-5')}}</h4>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="line-home">
        </div>
        <div class="text-container-bussiness">
            <ul>
                <li class="text-bussiness">
                    <p>
                        Company name
                        <span>
                                    SAKURA ECOLOGY CO.,LTD
                                </span>
                    </p>
                </li>
                <li class="text-bussiness">
                    <p> Establishment<br>December, 2007</p>
                </li>
                <li class="text-bussiness">
                    <p> Capital <br>1 5 0.0 0 0 USD </p>
                </li>
                <li class="text-bussiness">
                    <p> Representative<br> Director HIROO YAMADA</p>
                </li>
                <li class="text-bussiness">
                    <p>Address<br> No.96, 49 Street, Tan Quy Ward <br>Dist.7, Ho Chi Minh City, Vietnam<br> Tel:
                        (+84) 08-5433-0938 <br>Fax: (+84) 08-5433-0918</p>
                </li>
            </ul>
        </div>
    </section>
    <section>
        <div class=" footer-map ">
            <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.506676568423!2d106.79526881533452!3d10.84901436
                            0815055!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175273f7c6b6f0f%3A0x7cf04f5e668709b7!2zNjYgxJDGsOG7nW5nI
                            E1hbiBUaGnhu4duLCBQaMaw4budbmcgVMOibiBQaMO6LCBRdeG6rW4gOSwgSOG7kyBDaMOtIE1pbmgsIFZpZXRuYW0!5e0!3m2!1sen!2sus!4v1564469354462!5m2!1sen!2sus"
                    width="100%" height="500" frameborder="0" style="border:0" allowfullscreen>
            </iframe>
        </div>
    </section>
    @include('layouts.contact')
@endsection
