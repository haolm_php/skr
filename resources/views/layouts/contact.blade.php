<section class="footer-contact " id="contact">
    <div class="footer-contact contact">
        <div class="mb-3">
            <h2 class="text-center" style="margin: auto;">CONTACT US</h2>
        </div>
        <div class="row row-contacts">
            <div class="col-lg-6 col-md-6 col-sm-12 col-sx-12 col-form ">
                <div class="footer-warpper">
                    <div class="contain-contact">
                        <div class="row ">
                            <div class="col-md-6 col-sm-12 t-address">
                                <div class="colcontact-1 ">
                                    <div class="t-contact tt-contact ">
                                        <div class="text-name text-name-name">
                                            <p>SAKURA ECOLOGY CO.LTD</p>

                                        </div>
                                        <div class="text-brand">
                                            <p>Head Office</p>
                                        </div>
                                        <div class="text-address">
                                            <p class="d-flex">
                                                <span class=" mdi mdi-map-marker"></span>
                                                <span>No.96, 49 Street Tan Quy Ward Dist 7, Ho Chi Minh City, Vietnam</span>
                                            </p>
                                        </div>
                                        <div class="text-phone">
                                            <p class="d-flex">
                                                <span class=" mdi mdi-deskphone "></span>
                                                <span>Tel:(+84)08-5433-0938 </span>
                                            </p>
                                            <p class="d-flex">
                                                <span class=" mdi mdi-fax "></span>
                                                <span>Fax:(+84)08-5433-0918</span>
                                            </p>
                                        </div>
                                        <div class="text-email">
                                            <div class="t-foot text-t">
                                                <p class="d-flex">
                                                    <span class=" mdi mdi-email-outline"></span>
                                                    <span>secvn@sakuraecology.com</span>
                                                </p>
                                                <p class="d-flex text-t">
                                                    <span class=" mdi mdi-phone-in-talk"></span>
                                                    <span>Hotline: 028 54330938</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 t-address">
                                <div class="colcontact-1">
                                    <div class="t-contact t-contact-jp  ">
                                        <div class="text-name">
                                            <p>SAKURA ECOLOGY CO.LTD</p>
                                        </div>
                                        <div class="text-brand">
                                            <p>Brand</p>
                                        </div>
                                        <div class="text-address">
                                            <p><span class="mdi mdi-map-marker mdi-jp "></span> 32 Ai Mo, Bo De Ward, Long Bien Dist, Ha Noi city
                                               </p>
                                        </div>
                                        <div class="text-phone">
                                            <p><span class="mdi mdi-deskphone "></span>Tel:(+84)08-5433-0938 </p>
                                            <p><span class="mdi mdi-fax "></span>Fax:(+84)08-5433-0918</p>
                                        </div>
                                        <div class="text-email">
                                                    <span class="t-foot text-t">
                                                        <p class="d-flex ">
                                                            <span class=" mdi mdi-email-outline"></span>
                                                            <span>secvn@sakuraecology.com</span>
                                                        </p>
                                                        <p class="d-flex text-t">
                                                            <span class=" mdi mdi-phone-in-talk"></span>
                                                            <span>Hotline: 028 54330938</span>
                                                        </p>
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="icon-roll mobile" id="top-mobile">
                            <i class="mdi mdi-chevron-double-up"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-form-contact">
                <form action="{{route('contact_post')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row contain-contact">
                        <div class="col-md-6 colcontact ">
                            <div class="form-group text-form">
                                <input type="text" class="" id="name" placeholder="Name:" name="name"
                                       minlength="2"
                                       required="" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="colcontact">
                                <div class="form-group text-form">
                                    <input type="email" class="" id="uname" placeholder="Email:" name="email"
                                           required="" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group  text-form">
                        <textarea rows="6" cols="50" placeholder="Message" name="message" required=""></textarea>
                    </div>
                    <input type="submit" value="SEND">
                </form>
            </div>
        </div>
    </div>
</section>