<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('/img/page/favicon.png')}}" sizes="32x32">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/fontawesome.min.css')}}">
    <!--Owl Slider -->
    <link rel="stylesheet" href="{{asset('lib/OwlCarousel2/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('lib/OwlCarousel2/owl.theme.default.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/4.0.96/css/materialdesignicons.css">

    <!-- Style css -->
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <!-- Style css -->
    <link rel="stylesheet" href="{{asset('css/repository.css')}}">
    <!-- Style css -->
    <script>(function (d, s, id) {
            let js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=2488139114537079&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146851053-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-146851053-1');
    </script>
</head>
<body class="home">
<div class="wrapper">
    @yield('content')
    @include('layouts.footer')
</div>

<script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('lib/jquery/jquery.cookie.js')}}"></script>
<script src="{{asset('lib/OwlCarousel2/owl.carousel.min.js')}}"></script>
<script src="{{ asset('js/page.js') }}"></script>
@stack('page-scripts')
</body>
</html>
