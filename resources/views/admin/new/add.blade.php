@extends('admin.app')

@section('title', 'SKR | Dashboard')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item"><a href="{{url('/admin/news')}}" class="text-black">{{__('admin/layout.news_list')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.add_new')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{route('new_add_post')}}" method="POST" enctype="multipart/form-data"
                                  accept-charset="utf-8">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="slug">{{__('admin/layout.url')}}</label>
                                    <input id="slug" name="slug" type="text"
                                           value="{{old('slug')}}" class="form-control
                                                    @if($errors->has('slug')) border border-danger @endif"
                                           placeholder="Slug Url">
                                    @if ($errors->has('slug')) <span
                                            class="text-danger">{{$errors->first('slug')}}</span> @endif
                                </div>
                                <div class="card-body card card-primary ">
                                    <!-- Nav tabs -->
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home">{{__('admin/layout.vietnam')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#menu1">{{__('admin/layout.japan')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#menu2">{{__('admin/layout.english')}}</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div id="home" class=" tab-pane active"><br>
                                            <div class="form-group">
                                                <label for="tittles_vn">{{__('admin/layout.title')}}</label>
                                                <input id="tittles_vn" name="tittles_vn" type="text"
                                                       value="{{old('tittles_vn')}}" class="form-control
                                                    @if($errors->has('tittles_vn')) border border-danger @endif"
                                                       placeholder="Tittles Vietnam">
                                                @if ($errors->has('tittles_vn')) <span
                                                        class="text-danger">{{$errors->first('tittles_vn')}}</span> @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="contents_vn">{{__('admin/layout.content')}}</label>
                                                <textarea name="contents_vn" rows="20"
                                                          class="form-control  @if($errors->has('contents_vn'))
                                                                  dborder border-danger @endif">{{old('contents_vn')}}</textarea>
                                                @if ($errors->has('contents_vn'))
                                                    <span class="invalid-feedback" role="alert" style="color: red">
                                        <strong>{{ $errors->first('contents_vn') }}</strong>
                                      </span>
                                                @endif

                                                @if ($errors->has('contents_vn')) <span
                                                        class="text-danger">{{$errors->first('contents_vn')}}</span> @endif
                                            </div>
                                        </div>
                                        <div id="menu1" class=" tab-pane fade"><br>
                                            <div class="form-group">
                                                <label for="tittles_jp">{{__('admin/layout.title')}}</label>
                                                <input id="tittles_jp" name="tittles_jp" type="text"
                                                       value="{{old('tittles_jp')}}" class="form-control
                                                    @if($errors->has('tittles_jp')) border border-danger @endif"
                                                       placeholder="Tittle Japan">
                                                @if ($errors->has('tittles_jp')) <span
                                                        class="text-danger">{{$errors->first('tittles_jp')}}</span> @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="contents_jp">{{__('admin/layout.content')}}</label>
                                                <textarea name="contents_jp" rows="20"
                                                          class="form-control  @if($errors->has('contents_jp'))
                                                                  dborder border-danger @endif">{{old('contents_jp')}}</textarea>
                                                @if ($errors->has('contents_jp'))
                                                    <span class="invalid-feedback" role="alert" style="color: red">
                                        <strong>{{ $errors->first('contents_jp') }}</strong>
                                      </span>
                                                @endif

                                                @if ($errors->has('contents_jp')) <span
                                                        class="text-danger">{{$errors->first('contents_jp')}}</span> @endif
                                            </div>
                                        </div>
                                        <div id="menu2" class=" tab-pane fade"><br>
                                            <div class="form-group">
                                                <label for="tittles_en">{{__('admin/layout.title')}}</label>
                                                <input id="tittles_en" name="tittles_en" type="text"
                                                       value="{{old('tittles_en')}}" class="form-control
                                                    @if($errors->has('tittles_en')) border border-danger @endif"
                                                       placeholder="Tittle English">
                                                @if ($errors->has('tittles_en')) <span
                                                        class="text-danger">{{$errors->first('tittles_en')}}</span> @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="contents_en">{{__('admin/layout.content')}}</label>
                                                <textarea name="contents_en" rows="20"
                                                          class="form-control  @if($errors->has('contents_en'))
                                                                  dborder border-danger @endif">{{old('contents_en')}}</textarea>
                                                @if ($errors->has('contents_en'))
                                                    <span class="invalid-feedback" role="alert" style="color: red">
                                        <strong>{{ $errors->first('contents_en') }}</strong>
                                      </span>
                                                @endif

                                                @if ($errors->has('contents_en')) <span
                                                        class="text-danger">{{$errors->first('contents_en')}}</span> @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="file-upload ">
                                    <div class="file-select">
                                        <div class="file-select-button" id="fileName">{{__('admin/layout.choose_file')}}</div>
                                        <div class="file-select-name" id="noFile">{{__('admin/layout.file_not')}}</div>
                                        <input type="file" name="new_avatars" id="chooseFile" onchange="showimg(this);">

                                        @if ($errors->has('new_avatars')) <span
                                                class="text-danger">{{$errors->first('new_avatars')}}</span> @endif
                                    </div>
                                    <div class="input-img">
                                        <img id="nnavatar" src="" class="img-thumbnail"
                                             style="display: none; max-width: 150px">
                                    </div>
                                    <div class="clearfix">...</div>
                                </div>
                                <div class=" form-group ">
                                    <div class="d-flex justify-content-between bd-highlight mb-3 ">
                                        <div class="p-2 bd-highlight">
                                            <a href="{{url('admin/news')}}">
                                                <Button type="button" class="btn  btn-primary btn-outline-danger">
                                                    {{__('admin/layout.cancel')}}
                                                </Button>
                                            </a>
                                        </div>
                                        <div class="p-2 bd-highlight">
                                            <Button type="submit" class="btn  btn-primary ">{{__('admin/layout.created_btn')}}</Button>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- ckeditor config -->
    <script src="{{asset('lib/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace('contents_vn');
        CKEDITOR.replace('contents_jp');
        CKEDITOR.replace('contents_en');
    </script>
@endsection
