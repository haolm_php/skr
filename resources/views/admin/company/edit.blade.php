@extends('admin.app')
@section('title', 'SKR | Company')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item "><a href="{{url('/admin/company')}}" class="text-black">{{__('admin/layout.company_c_list')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.edit_company')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{route('post_company_edit',$company['id'])}}" method="POST" enctype="multipart/form-data"
                                  accept-charset="utf-8">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="company_name">{{__('admin/layout.name')}}</label>
                                            <input id="company_name" name="name" type="text" required
                                                   value="{{old('name',$company['name'])}}" class="form-control
                                                    @if($errors->has('name')) border border-danger @endif"
                                                   placeholder="Name">
                                            @if ($errors->has('name')) <span
                                                    class="text-danger">{{$errors->first('name')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="company_code">{{__('admin/layout.code')}}</label>
                                            <input id="company_code" name="code" type="text" required
                                                   value="{{old('code',$company['code'])}}" class="form-control
                                                    @if($errors->has('code')) border border-danger @endif"
                                                   placeholder="Code Company">
                                            @if ($errors->has('code')) <span
                                                    class="text-danger">{{$errors->first('code')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="mail_footer_jp">{{__('admin/layout.mail_footer_jp')}}</label>
                                            <textarea name="mail_footer_jp" rows="4" required placeholder="Thanks You contact mail Japanese"
                                                      class="form-control  @if($errors->has('mail_footer_jp'))
                                                              dborder border-danger @endif">{{old('mail_footer_jp',$company['mail_footer_jp'])}}</textarea>
                                            @if ($errors->has('mail_footer_jp')) <span
                                                    class="text-danger">{{$errors->first('mail_footer_jp',$company['mail_footer_jp'])}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="mail_footer_en">{{__('admin/layout.mail_footer_en')}}</label>
                                            <textarea name="mail_footer_en" rows="4" required placeholder="Thanks You contact mail English"
                                                      class="form-control  @if($errors->has('mail_footer_en'))
                                                              dborder border-danger @endif">{{old('mail_footer_en',$company['mail_footer_en'])}}</textarea>
                                            @if ($errors->has('mail_footer_en')) <span
                                                    class="text-danger">{{$errors->first('mail_footer_en')}}
                                            </span> @endif
                                        </div>

                                        <div class="file-upload ">
                                            <div class="file-select">
                                                <div class="file-select-button" id="fileName">{{__('admin/layout.choose_file')}}</div>
                                                <div class="file-select-name" id="noFile">{{$company['logo']}}</div>
                                                <input type="file" name="logo" id="chooseFile" onchange="showimg(this);">

                                                @if ($errors->has('logo')) <span
                                                        class="text-danger">{{$errors->first('logo')}}</span> @endif
                                            </div>
                                            <div class="input-img">
                                                <img class="new-av" src="{{asset('img/upload/'.$company['logo'])}}"
                                                     class="img-thumbnail"
                                                     style="max-width: 150px">
                                                <img id="nnavatar" src="" class="img-thumbnail"
                                                     style="display: none; max-width: 150px">
                                            </div>
                                            <div class="clearfix">...</div>
                                        </div>
                                        <div class="d-flex justify-content-between bd-highlight mb-3">
                                            <div class="p-2 bd-highlight">
                                                <a href="{{url('admin/company')}}">
                                                    <Button type="button" class="btn  btn-primary btn-outline-danger">
                                                        {{__('admin/layout.cancel')}}
                                                    </Button>
                                                </a>
                                            </div>
                                            <div class="p-2 bd-highlight">
                                                <Button type="submit" class="btn  btn-primary ">{{__('admin/layout.update')}}</Button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
