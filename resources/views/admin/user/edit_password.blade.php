@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item"><a href="{{url('/admin/user')}}" class="text-black">{{__('admin/layout.news_list')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.edit_password')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{route('user_edit_pass_post',$id)}}" method="POST" enctype="multipart/form-data"
                                  accept-charset="utf-8">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="password">{{__('admin/layout.password')}}</label>
                                            <input id="password" name="password" type="password"
                                                   value="{{old('password')}}" class="form-control
                                                    @if($errors->has('password')) border border-danger @endif"
                                                   placeholder="Password">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert" style="color: red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                            @endif
                                            @if ($errors->has('password')) <span
                                                    class="text-danger">{{$errors->first('password')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="cf_password">{{__('admin/layout.c_password')}}</label>
                                            <input id="cf_password" name="cf_password" type="password"
                                                   value="{{old('cf_password')}}" class="form-control
                                                    @if($errors->has('cf_password')) border border-danger @endif"
                                                   placeholder="Confirm Password">
                                            @if ($errors->has('cf_password'))
                                                <span class="invalid-feedback" role="alert" style="color: red">
                                        <strong>{{ $errors->first('cf_password') }}</strong>
                                      </span>
                                            @endif
                                            @if ($errors->has('cf_password')) <span
                                                    class="text-danger">{{$errors->first('cf_password')}}</span> @endif
                                        </div>
                                        <div class="d-flex justify-content-between bd-highlight mb-3">
                                            <div class="p-2 bd-highlight">
                                                <a href="{{url('admin/user')}}">
                                                    <Button type="button" class="btn  btn-primary btn-outline-danger">
                                                        {{__('admin/layout.cancel')}}
                                                    </Button>
                                                </a>
                                            </div>
                                            <div class="p-2 bd-highlight">
                                                <Button type="submit" class="btn  btn-primary ">{{__('admin/layout.reset')}}</Button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
