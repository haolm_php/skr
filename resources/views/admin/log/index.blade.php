@extends('admin.app')
@section('title', 'SKR | Activity Log')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.log_list')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong> {{session('status')}}</strong>
                            </div>
                        @endif
                        <div id="snoAlertBox" class="alert alert-success" data-alert="alert">Now Update your Search</div>
                        <div class="card card-primary card-outline">
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="log_list" class="table table-bordered table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin/layout.user')}}</th>
                                            <th>{{__('admin/layout.ip')}}</th>
                                            <th>{{__('admin/layout.history')}}</th>
                                            <th>{{__('admin/layout.log_time')}}</th>
                                            <th class="btn-action" style="width: 700px">{{__('admin/layout.more')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </section>
        <!-- /.content -->
    </div>

@endsection
@push('page-scripts')
    <!-- DataTables -->
    <script>
        $(document).ready(function() {
            $('table').on('click', function(e){
                if($('.popoverButton').length>1)
                    $('.popoverButton').popover('hide');
                $(e.target).popover('toggle');
            });

            let url=" ";
            @if(session('lang')=='jp')
                url ="{{asset('lib/datatables/table.json')}}";
            @endif
                $.fn.dataTable.ext.errMode = 'throw';
            $("#log_list").DataTable({
                "language": {
                    url: url
                },
                "processing": true,
                "lengthChange": true,
                "pageLength": 10,
                "serverSide": true,
                "ajax": {
                    "url": "{{route('api.log.index')}}",
                    "type": 'GET',
                },
                "columns": [
                    {
                        "data": "name",
                    },
                    {
                        "data": "properties",
                        "render": function(data, type, row) {
                            let ip =JSON.parse(data.replace(/&quot;/g,'"')).ip;
                            return ip;
                        }
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "created_at"
                    },
                ],
                "columnDefs": [{
                    "targets": 4,
                    "data": "properties",
                    "render": function(data, type, row) {
                        let browser =JSON.parse(data.replace(/&quot;/g,'"')).browser;
                        return browser;
                    }
                }],
                "order": [ 3, "desc" ],

            });
        });
    </script>

@endpush

