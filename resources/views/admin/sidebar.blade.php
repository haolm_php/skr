<!-- ./Vung Header -->
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('img/avatar.jpg')}}" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i>
                    {{__('admin/layout.online')}}
                </a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">{{__('admin/layout.function')}}</li>
            <li>
                <a href="{{ url('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i> {{__('admin/layout.dashboard')}}
                </a>
            </li>
            <li>
                <a href="{{ url('admin/contact') }}">
                    <i class="fa fa-envelope"></i> {{__('admin/layout.contact')}}
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/categories') }}">
                    <i class="fa fa-indent"></i><span>{{__('admin/layout.categories')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('admin/news') }}">
                    <i class="fa fa-newspaper-o"></i><span>{{__('admin/layout.news')}}</span>
                </a>
            </li>
            <li class="header">{{__('admin/layout.company')}}</li>
            <li class="treeview">
                <a href="{{ url('admin/company') }}">
                    <i class="fa fa-building"></i><span>{{__('admin/layout.company')}}</span>
                </a>
            </li>

            <li class="">
                <a href="{{ url('admin/company-contact') }}">
                    <i class="fa fa-address-book"></i><span>{{__('admin/layout.company_contact')}} </span>
                </a>
            </li>
            <li class="header">{{__('admin/layout.account')}}</li>
            <li class="">
                <a href="{{ url('admin/user') }}">
                    <i class="fa fa-user"></i><span>{{__('admin/layout.user')}}</span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('admin/roles') }}">
                    <i class="fa fa-user-circle-o"></i><span>{{__('admin/layout.roles_per')}}</span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('admin/activity') }}">
                    <i class="fa fa-server"></i><span>{{__('admin/layout.log')}}</span>
                </a>
            </li>
            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                            class="fa fa-sign-out"></i>
                    {{__('admin/layout.logout')}}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>

    </section>
    <script>
        $(document).ready(function () {
            var url = window.location;
            var element = $('ul.sidebar-menu a').filter(function () {
                return this.href == url || window.location.pathname == 0;
            });
            $(element).parentsUntil('ul.sidebar-menu', 'li').addClass('active');
        });
    </script>
</aside>