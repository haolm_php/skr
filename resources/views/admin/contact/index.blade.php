@extends('admin.app')

@section('title', 'SKR | Dashboard')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.contact_list')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible" id="status">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{__('admin/layout.success')}}</strong> {{session('status')}}
                        </div>
                    @endif
                    <div class="col-12">

                        <div class="card card-primary card-outline">
                            <h3 class="text-center p-2">Contact Message</h3>
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="contact_list" class="table table-bordered table-hover" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin/layout.name')}}</th>
                                            <th>{{__('admin/layout.email')}}</th>
                                            <th> {{__('admin/layout.created')}}</th>
                                            <th>{{__('admin/layout.status')}}</th>
                                            <th class="btn-action">{{__('admin/layout.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-12">

                        <div class="card card-primary card-outline">
                            <h3 class="text-center p-2">Contact Recruitment</h3>
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="contact_list_relay" class="table table-bordered table-hover" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin/layout.name')}}</th>
                                            <th>{{__('admin/layout.email')}}</th>
                                            <th> {{__('admin/layout.created')}}</th>
                                            <th>{{__('admin/layout.status')}}</th>
                                            <th class="btn-action">{{__('admin/layout.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <!-- confirm dialog-->
            <div class="modal fade" id="confirmDeleteContact" tabindex="-1" role="dialog" aria-labelledby="modelLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                           {{__('admin/layout.confirm_d')}}
                        </div>
                        <div id="confirmMessage" class="modal-body">
                            {{__('admin/layout.delete_c')}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok" onclick="deleteContact($(this).val())">
                                {{__('admin/layout.delete')}}
                            </button>
                            <button type="button" id="confirmCancel" class="btn btn-cancel" data-dismiss="modal"  data-toggle="modal" data-target="#confirmDeleteContact">
                                {{__('admin/layout.cancel')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>


@endsection
@push('page-scripts')
    <!-- DataTables -->

    <script>
        function checkContactExisted(id) {
            $('#btnConfirmDelete').val(id);
        }
        function deleteContact(id) {
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('contact_delete')}}",
                method: "post",
                data: {
                    Id: id
                }
            }).done(function(data) {
                if (data == 0) {
                    location.reload(true);
                } else {
                    $("#confirmDeleteNew").modal('hide');
                }
            })
        }
        $(document).ready(function() {
            let url=" ";
            let detail_btn ="{{__('admin/layout.detail')}}";
            let delete_btn="{{__('admin/layout.delete')}}";
            @if(session('lang')=='jp')
                url ="{{asset('lib/datatables/table.json')}}";
            @endif
                $.fn.dataTable.ext.errMode = 'throw';
            $("#contact_list").DataTable({
                "language": {
                    url: url
                },
                "processing": true,
                "lengthChange": true,
                "pageLength": 15,
                "serverSide": true,
                "ajax": {
                    "url": "{{route('api.contact.index')}}",
                    "type": 'GET',
                },
                "columns": [
                    {
                        "data": "name"
                    },
                    {
                        "data": "email"
                    },
                    {
                        "data": "created_at"

                    },
                    {
                        "data": "status",
                    },
                ],
                "columnDefs": [{
                    "targets": 4,
                    "data": null,
                    "render": function(data, type, row, meta) {
                        edit_href = "{{url('admin/contact/detail')}}/" + data.id;
                        delete_href = ' onclick = checkContactExisted(' + data.id + ') ';
                        return '<button class="btn btn-success btn-xs" ><a href=' + edit_href + '><span class="fa fa-edit"></span> '+detail_btn+'</a></button>'+
                            '<button class="btn btn-xs btn-danger delete-contact"  '+delete_href +' data-toggle="modal"' +
                            ' data-target="#confirmDeleteContact" <span class="fa fa-trash" > </span> '+delete_btn+'</a></button>'
                    }
                }],
                 "order": [ 2, "desc" ],
            });
        });

    </script>

    @endpush

