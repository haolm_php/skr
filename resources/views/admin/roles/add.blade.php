@extends('admin.app')

@section('title', 'SKR | Users')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}"
                                                                                      class="text-black">{{__('admin/layout.home')}}</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{url('/admin/roles')}}"
                                                           class="text-black">{{__('admin/layout.roles')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.add_roles')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- Main content -->
        <section class="content">
            <form action="{{route('roles_add_post')}}" method="POST" enctype="multipart/form-data"  accept-charset="utf-8">
            <div class="row">
                <div class="col-8">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="tittle_new">{{__('admin/layout.name')}}</label>
                                            <input id="tittle_new" name="name" type="text"
                                                   value="{{old('name')}}" class="form-control
                                                    @if($errors->has('name')) border border-danger @endif"
                                                   placeholder="Name">
                                            @if ($errors->has('name')) <span
                                                    class="text-danger">{{$errors->first('name')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="description">{{__('admin/layout.description')}} </label>
                                            <textarea name="description" rows="4" required
                                                      placeholder="Short description"
                                                      class="form-control  @if($errors->has('description'))
                                                              dborder border-danger @endif">{{old('description')}}</textarea>
                                            @if ($errors->has('description')) <span
                                                    class="text-danger">{{$errors->first('description')}}</span> @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row container">
                                    <div class="widget meta-boxes">
                                        <div class="widget-title">
                                            <h4>
                                                <span> {{__('admin/layout.per_flags')}}</span>
                                            </h4>
                                        </div>
                                        <div class="widget-body">
                                            <ul id="auto-checkboxes" data-name="foo" class="list-unstyled list-feature">
                                                <li id="mainNode" class="ui-widget ui-widget-content daredevel-tree">

                                                    <input type="checkbox" class="hrv-checkbox" id="expandCollapseAllTree">&nbsp;&nbsp;
                                                    <label for="expandCollapseAllTree" class="label label-default allTree">{{__('admin/layout.per_all')}}</label>
                                                    <ul class="check-roles">
                                                        @foreach ($permission as $key => $value)
                                                            <li class="ui-widget ui-widget-content daredevel-tree expanded" id="node4">
                                                                <input type="checkbox" class="hrv-checkbox" id="checkSelect4" name="flags[]" value="{{$key}}">
                                                                <label for="checkSelect0" class="label label-warning" style="margin: 5px;">{{$value}}</label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-4">
                    <div class="card card-primary card-outline">
                        <div class="widget-title p-2 border-bottom">
                            <h4>
                                <span>{{__('admin/layout.action')}}</span>
                            </h4>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="d-flex justify-content-between bd-highlight mb-3">
                                <div class="bd-highlight">
                                    <a href="{{url('admin/roles')}}" class="p-2">
                                        <Button type="button" class="btn  btn-primary btn-outline-danger">
                                            {{__('admin/layout.cancel')}}
                                        </Button>
                                    </a>
                                    <Button type="submit"
                                            class="btn  btn-primary ">{{__('admin/layout.created_btn')}}</Button>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            </form>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script>
        $("#expandCollapseAllTree").click(function(){
            $('.check-roles input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
@endsection
