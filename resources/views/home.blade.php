@extends('layouts.app')
@section('content')
    @include('sweetalert::alert')
    @include('layouts.header')
    <div class="content-main">
        <section id="contain-about">
            <div class="slideshow-container">
                <div class="mySlides fade">
                    <div class="owl-carousel owl-theme slider-main">
                        <div class="item">
                            <img src="{{asset('/img/sakuraimage/slider1.png')}}" alt="">
                            <div class="contain-slider">
                                <h3>Ensuring the quality of service Sakura
                                    company wishes to accompany customers<br>
                                    <span>Sakura Ecology Company</span>
                                    hopes to make customers satisfied with some our servies.</h3>
                            </div>
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/sakuraimage/slider2.png')}}" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <div class="skr-container content">
        <div class="row-about">
            <div class="mb-3 colabout mobile">
                <h2 class=" text-center ">{{__('home.about-us')}}</h2>
            </div>
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                    <div class="colabout colabot">
                        <div class="about-us">
                            <img src="{{asset('img/sakuraimage/aboutas.png')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                    <div class="colabout colabout-child">
                        <h2 class=" text-left desktop">{{__('home.about-us')}}</h2>
                        <p>{{__('home.about-content1')}}
                            <span class="about-mobile">
                                     {{__('home.about-content2')}}
                                    </span>
                        </p>
                        <p class="about-mobile mobile ">{{__('home.about-thanks')}}</p>
                        <p class="desktop">{{__('home.about-thanks')}}</p>
                        <div class="btn-see-about text-center mobile">
                            <span>{{__('home.read-more')}} <i>▾</i></span></div>
                        <div class="btn-compact-about text-center mobile" style="display: none;">
                            <span>{{__('home.compact')}}<i>▴</i></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <div class="content-main content">
        <section id="contain-intro">
            <div class="row-intro">
                <div class="colintro">
                    <a href="{{url('/business-outline')}}">
                        <div class="intro-img">
                            <img src="{{asset('/img/sakuraimage/bussinessoutline.png')}}" alt="Business outline">
                        </div>
                        <div class="img-muiten">
                            <h3>{{__('home.business')}}</h3>
                            <p>{{__('home.read-more')}}</p>
                            <img src="{{asset('/img/icon/icon4.png')}}" alt="">
                        </div>
                    </a>
                </div>
                <div class="colintro">
                    <a href="{{url('/principles')}}">
                        <div class="intro-img">
                            <img src="{{asset('/img/sakuraimage/guidingprinciple.png')}}" alt="Guiding principle">
                        </div>
                        <div class="img-muiten">
                            <h3> {{__('home.guiding')}}</h3>
                            <p>{{__('home.read-more')}}</p>
                            <img src="{{asset('/img/icon/icon4.png')}}" alt="Guiding Principles">
                        </div>
                    </a>
                </div>
                <div class="colintro">
                    <a href="{{url('/execution-order')}}">
                        <div class="intro-img">
                            <img src="{{asset('/img/sakuraimage/executionorder.png')}}" alt="Execution order">
                        </div>
                        <div class="img-muiten">
                            <h3> {{__('home.execution')}}</h3>
                            <p>{{__('home.read-more')}}</p>
                            <img src="{{asset('/img/icon/icon4.png')}}" alt="Execution order">
                        </div>
                    </a>
                </div>
            </div>
        </section>
        <section class="content-home text-center all-services" id="services">
            <div class="content-home1 colabout">
                <div class="mb-3">
                    <h2 class="text-center" style="margin: auto;">{{__('home.all-sv')}}</h2>
                </div>
            </div>
            <!-- Nav tabs -->
            <div class="conten-services nav-tabs-wrapper" id="apps-tab-downloaded">
                <ul class="nav nav-tabs " role="tablist">
                    <li class="col col1 nav-item ">
                        <a class="nav-link active" data-toggle="tab" href="#menu0">{{__('home.operation')}}</a>
                    </li>
                    <li class="col col1 nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu1">{{__('home.office')}}</a>
                    </li>
                    <li class="col col1 nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu6">{{__('home.supermarket-d')}}</a>
                    </li>
                    <li class="col col12 nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu2">{{__('home.apartment')}}</a>
                    </li>
                    <li class="col col1 nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu4">{{__('home.factory')}}</a>
                    </li>
                    <li class="col col1 nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu3">{{__('home.planting')}}</a>
                    </li>
                    <li class="col col1 nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu5">{{__('home.clean')}}</a>
                    </li>

                </ul>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div id="menu0" class="text-center  tab-pane  tab-pane1 active"><br>
                    <div class="row row-tabs">
                    </div>
                </div>
                <div id="menu1" class="text-center  tab-pane  tab-pane1 "><br>
                    <h1 class="">Office Building</h1>
                    <img src="{{asset('img/sakuraimage/Office.png')}}">
                    <div class="row row-tabs ">
                        <div class="col-md-3 col-6 new-office">
                            <div class="text-h4">
                                <h5>{{__('home.office-t1')}}</h5>
                            </div>
                            <p>{{__('home.office-c1')}}</p>
                        </div>
                        <div class="col-md-3 col-6 new-office ">
                            <div class="text-h4">
                                <h5>{{__('home.office-t2')}}</h5>
                            </div>
                            <p>{{__('home.office-c2')}}</p>
                        </div>
                        <div class="col-md-3 col-6 new-office">
                            <div class="text-h4">
                                <h5>{{__('home.office-t3')}}</h5>
                            </div>
                            <p>{{__('home.office-c3')}}</p>
                        </div>
                        <div class="col-md-3 col-6 new-office">
                            <div class="text-h4">
                                <h5>{{__('home.office-t4')}}</h5>
                            </div>
                            <p>{{__('home.office-c4')}}.</p>
                        </div>
                    </div>
                </div>
                <div id="menu6" class=" tab-pane tab-pane5 fade"><br>
                    <div class="row row-tabs">
                        <div class="col-md-5 col-sm-12 coltab text-text">
                            <h1 class="operation">{{__('home.supermarket')}} <br> {{__('home.department')}}</h1>
                            <img src="{{asset('img/sakuraimage/Supermarket.png')}}" alt="Supermarket">
                        </div>
                        <div class="col-md-7 col-sm-12 coltab text-left">
                            <div class="row row-bottom">
                                <div class="col-md-3 col-2 ">
                                    <div class="circle"><span>01</span></div>
                                </div>
                                <div class="col-md-9 col-10">
                                    <p><strong>{{__('home.supermarket-t1')}} </strong>{{__('home.supermarket-c1')}}.</p>
                                </div>
                            </div>
                            <div class="row row-bottom">
                                <div class="col-md-3 col-2 ">
                                    <div class="circle circle-purple"><span>02</span></div>
                                </div>
                                <div class="col-md-9 col-10">
                                    <p><strong>{{__('home.supermarket-t2')}} </strong>{{__('home.supermarket-c2')}}.</p>
                                </div>
                            </div>
                            <div class="row row-bottom">
                                <div class="col-md-3 col-2 ">
                                    <div class="circle"><span>03</span></div>
                                </div>
                                <div class="col-md-9 col-10">
                                    <p><strong>{{__('home.supermarket-t3')}} </strong>{{__('home.supermarket-c3')}}.</p>
                                </div>
                            </div>
                            <div class="row row-bottom">
                                <div class="col-md-3 col-2 ">
                                    <div class="circle circle-purple"><span>04</span></div>
                                </div>
                                <div class="col-md-9 col-10">
                                    <p><strong>{{__('home.supermarket-t4')}} </strong>{{__('home.supermarket-c4')}}.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu2" class=" tab-pane fade"><br>
                    <div class="header-menu-2">
                        <h1>{{__('home.apartment')}}</h1>
                    </div>
                    <div class="row row-tabs">
                        <div class="col col1 operation-black">
                            <div class="text-menu-2">
                                <h5>{{__('home.apartment-t1')}}</h5>
                                <p>{{__('home.apartment-c1')}}</p>
                            </div>
                        </div>
                        <div class="col col2 operation-black">
                            <div class="text-menu-2">
                                <h5>{{__('home.apartment-t2')}}</h5>
                                <p>{{__('home.apartment-c2')}}</p>
                            </div>
                        </div>
                        <div class="col col3 operation-black">
                            <div class="text-menu-2">
                                <h5>{{__('home.apartment-t3')}}</h5>
                                <p>{{__('home.apartment-c3')}}</p>
                            </div>
                        </div>
                        <div class="col col4 operation-black">
                            <div class="text-menu-2">
                                <h5>{{__('home.apartment-t4')}}</h5>
                                <p>{{__('home.apartment-c4')}}</p>
                            </div>
                        </div>
                        <div class="col col5 operation-black">
                            <div class="text-menu-2">
                                <h5>{{__('home.apartment-t5')}}</h5>
                                <p>{{__('home.apartment-c5')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu4" class=" tab-pane tab-pane3 fade"><br>
                    <div class="row row-tabs">
                        <div class="col-md-6 col-sm-12 ">

                            <div class="factories">
                                <div class="operation-border">
                                </div>
                                <img src="{{asset('img/sakuraimage/Factory1.png')}}" alt="Factory">
                                <span class="">{{__('home.factory')}}</span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-sx-12  text-left factories-item">
                            <div class="row ">
                                <div class="col-2 coltab-circle">
                                    <div class="circle"><span>01</span></div>
                                    <div class="circle"><span>02</span></div>
                                    <div class="circle"><span>03</span></div>
                                    <div class="circle"><span>04</span></div>
                                </div>
                                <div class="col-10 factories-right">
                                    <p><strong>{{__('home.factory-t1')}} </strong>{{__('home.factory-c1')}}</p>
                                    <p><strong>{{__('home.factory-t2')}} </strong>{{__('home.factory-c2')}}</p>
                                    <p><strong>{{__('home.factory-t3')}} </strong>{{__('home.factory-c3')}}</p>
                                    <p><strong>{{__('home.factory-t4')}} </strong>{{__('home.factory-c4')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu3" class=" tab-pane tab-pane2 fade"><br>
                    <h1>{{__('home.planting')}}</h1>
                    <div class="planting-service">
                        <div class="operation-black-0 operation-black-9">
                            <h4>{{__('home.planting-t1')}}</h4>
                            <div class="operation-bottom">{{__('home.planting-c1')}}</div>
                        </div>
                        <div class="operation-black-0 operation-black-4">
                            <img src="{{asset('img/sakuraimage/arrow1.png')}}" alt="">
                        </div>
                        <div class="operation-black-0 operation-black-1">
                            <h4>{{__('home.planting-t2')}}</h4>
                            <div class="operation-bottom">{{__('home.planting-c2')}}</div>
                        </div>
                        <div class="operation-black-0 operation-black-6">
                            <img src="{{asset('img/sakuraimage/arrow2.png')}}" alt="">
                        </div>
                        <div class="operation-black-0 operation-black-2">
                            <h4>{{__('home.planting-t3')}}</h4>
                            <div class="operation-bottom">{{__('home.planting-c3')}}</div>
                        </div>
                        <div class="operation-black-0 operation-black-8">
                            <img src="{{asset('img/sakuraimage/arrow3.png')}}" alt="">
                        </div>
                        <div class="operation-black-0 operation-black-3">
                            <h4>{{__('home.planting-t4')}}</h4>
                            <div class="operation-bottom">{{__('home.planting-c4')}}</div>
                        </div>
                        <div class="operation-black-0 operation-black-5">
                            <img src="{{asset('img/sakuraimage/arrow4.png')}}" alt="">
                        </div>
                        <div class="operation-black-0 operation-black-7">
                            <img src="{{asset('img/sakuraimage/Plantingservice.png')}}" alt="">
                        </div>
                    </div>
                </div>
                <div id="menu5" class=" tab-pane tab-pane4 fade"><br>
                    <h1>{{__('home.clean')}} </h1>
                    <div class="hotel">
                        <div class="row hotel-tab">
                            <div class="col-lg-3 col-md-3 col-sm-12 coltab hotel-operation">
                                <h5 class="operation"> {{__('home.clean-t1')}} </h5>
                                <p>{{__('home.clean-c1')}} </p>
                            </div>
                            <div class="col-md-3 col-12 hotel-mobile ">
                                <img src="{{asset('img/sakuraimage/Cleanroom3.png')}}" alt="">
                            </div>
                            <div class="col-md-3 col-12 hotel-mobile-1 ">
                                <img src="{{asset('img/sakuraimage/Cleanroom5.png')}}" alt="">
                            </div>
                            <div class="col-md-3 col-12  ">
                                <h5 class="operation"> {{__('home.clean-t2')}} </h5>
                                <p>{{__('home.clean-c2')}} </p>
                            </div>
                            <!-- End Tab panes -->
                            <!-- End Nav tabs -->
                        </div>
                        <div class="row hotel-tab">
                            <div class="col-lg-3 col-md-3 col-sm-12 coltab hotel-operation">
                                <h5 class="operation"> {{__('home.clean-t3')}} </h5>
                                <p>{{__('home.clean-c3')}} </p>
                            </div>
                            <div class="col-md-3 col-12 hotel-mobile ">
                                <img src="{{asset('img/sakuraimage/Cleanroom1.png')}}" alt="">

                            </div>
                            <div class="col-md-3 col-12 hotel-mobile-1">
                                <img src="{{asset('img/sakuraimage/Cleanroom2.png')}}" alt="">
                            </div>
                            <div class="col-md-3 col-12  ">
                                <h5 class="operation"> {{__('home.clean-t4')}} </h5>
                                <p>{{__('home.clean-c4')}} </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="contain-hightline">
            <div class="skr-container">
                <div class="hightline-services">
                    <div class="text-services">
                        <h2>{{__('home.h-service')}} </h2>
                        <h3>{{__('home.h-service-title')}} </h3>
                        <p>{{__('home.h-service-content')}}</p>
                    </div>
                    <div class="owl-carousel owl-theme slider-service owl-loaded owl-drag">
                        <div class="owl-stage-outer">
                            <div class="owl-stage"
                                 style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2138px;">
                                <div class="owl-item active" style="width: 356.325px;">
                                    <div class="item">
                                        <img src="{{asset('img/sakuraimage/cleaning1.png')}}" alt="">
                                        <div class="text-imgservices">
                                            <h3>FLOOR CLEANING</h3>
                                            <p>clean and beautiful space for customers.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item active" style="width: 356.325px;">
                                    <div class="item">
                                        <img src="{{asset('img/sakuraimage/cleaning2.png')}}" alt="">
                                        <div class="text-imgservices">
                                            <h3>GLASS CLEANING</h3>
                                            <p>clean and beautiful space for customers.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="owl-item active" style="width: 356.325px;">
                                    <div class="item">
                                        <img src="{{asset('/img/sakuraimage/Bonsai.png')}}" alt="">
                                        <div class="text-imgservices">
                                            <h3>BONSAI CARE</h3>
                                            <p>clean and beautiful space for customers.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 356.325px;">
                                    <div class="item">
                                        <img src="{{asset('/img/newpage/Petscontrol.png')}}" alt="">
                                        <div class="text-imgservices">
                                            <h3>PETS CONTROL</h3>
                                            <p>clean and beautiful space for customers.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="content-news">
            <div class="row-news">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 new-left">
                        <div class="colnews">
                            <h2>{{__('home.feedback')}}</h2>
                            <div class="owl-carousel owl-theme slider-news">
                                <div class="item">
                                    <p class="news-p">molestie. Sed sed ullamcorper lorem, id faucibus odio.
                                        eu nisl ut ligula cursus molestie at at dolor. Nulla est justo,
                                        pellentesque vel lectus eget, fermentum varius dui. Morbi faucibus
                                        quam sed efficitur interdum.</p>
                                    <img src="{{asset('/img/newpage/downlist1.png')}}" alt="">
                                </div>
                                <div class="item">
                                    <p class="news-p">molestie. Sed sed ullamcorper lorem, id faucibus odio.
                                        eu nisl ut ligula cursus molestie at at dolor. Nulla est justo,
                                        pellentesque vel lectus eget, fermentum varius dui. Morbi faucibus
                                        quam sed efficitur interdum.</p>
                                    <img src="{{asset('/img/newpage/class1.png')}}" alt="">
                                </div>
                                <div class="item">
                                    <p class="news-p">molestie. Sed sed ullamcorper lorem, id faucibus odio.
                                        eu nisl ut ligula cursus molestie at at dolor. Nulla est justo,
                                        pellentesque vel lectus eget, fermentum varius dui. Morbi faucibus
                                        quam sed efficitur interdum.</p>
                                    <img src="{{asset('/img/newpage/ve-sinh-van-phong.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class=" colnews row-15">
                            <div class="news">
                                <h2>{{__('home.news')}}</h2>
                                <div class="slider_nav">
                                    <button class="am-prev"><span>{{__('home.next')}}</span><img
                                                src="{{asset('/img/icon/next_new.png')}}" alt="">
                                    </button>
                                </div>
                            </div>
                            <div class="owl-carousel owl-theme slider-news-1">
                                @foreach($new as $news)
                                    <div class="item">
                                        <div class="colnews  colnews-child">
                                            <div class="child-news">
                                                <a href="{{url('/news')}}/{{$news->slug}}">
                                                    <img src="{{asset('img/upload/')}}/{{$news->avatar}}" alt="">
                                                </a>
                                                <div class="container-news">
                                                    @if (session('lang')=='vi' )
                                                        <a href="{{url('/news')}}/{{$news->slug}}"
                                                           class="txt_wg_title" target="_blank" rel=""
                                                           title="{{$news->tittle_vn}}">{{$news->tittle_vn}}</a>
                                                    @elseif (session('lang')=='jp' )
                                                        <a href="{{url('/news')}}/{{$news->slug}}"
                                                           class="txt_wg_title" target="_blank" rel=""
                                                           title="{{$news->tittle_jp}}">{{$news->tittle_jp}}</a>
                                                    @else
                                                        <a href="{{url('/news')}}/{{$news->slug}}"
                                                           class="txt_wg_title" target="_blank" rel=""
                                                           title="{{$news->tittle_en}}">{{$news->tittle_en}}</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="date-news">
                                            <span class="mdi mdi-clock" style="color: #8e1ea2"></span>
                                            <span>{{ \Carbon\Carbon::parse($news->created_at)->format('d/m/Y')}}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="logo" id="customer">

            <div class="container-logo ">
                <div class="text-left border-bottom row">
                    <div class="select">
                        <ul>
                            <li class="option customer-vn">
                                {{__('home.vn-company')}}
                            </li>
                            <li class="option customer-jp">
                                {{__('home.jp-company')}}
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="customer-box">
                    <div class="owl-carousel owl-theme slider-logo customer-vn active" id="customer-vn">
                        <div class="item">
                            <img src="{{asset('/img/page/8.jpg')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/10.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/13.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/11.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/9.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/7.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/14.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/2.png')}}" alt="">
                        </div>
                    </div>
                    <div class="owl-carousel owl-theme slider-logo customer-jp" id="customer-jp">
                        <div class="item">
                            <img src="{{asset('/img/page/10.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/8.jpg')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/13.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/11.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/9.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/7.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/14.png')}}" alt="">
                        </div>
                        <div class="item">
                            <img src="{{asset('/img/page/2.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @include('layouts.contact')
@endsection


