<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'function' => 'FUNCTION',
    'dashboard' => 'Dashboard',
    'contact' => 'Contact',
    'account'=>'Account',
    'news' => 'News',
    'company' => 'company',
    'company_contact' => 'Company Contact',
    'user' => 'User',
    'log' => 'Activity Log',
    'logout' => 'Logout',
    'online' => 'online',
    'home'=>'Home',
    'web_index' => 'view website',
    'more'=>'More Info',
    'news_chars' => 'Chart News',
    'contact_chars'=>'Chart Contact',
    'message_noti'=>'You Have New Message',
    'view_all'=>'View All',
    // Contact
    'contact_list' => 'Contact List',
    'name' => 'Name',
    'email' => 'Email',
    'created' => 'Created At',
    'status' => 'Status',
    'mail_n' => 'UnRead',
    'mail_r' => '読む',
    'mail_s' => 'メールを送る',
    'edit'=>'Edit',
    'delete' => 'Delete',
    'action'=>'Action',
    'cancel' => 'Cancel',
    'relay' => 'Relay',
    'confirm_d'=>'Confirm Delete',
    'delete_c'=>'Do you contacts want to delete this record ?',
    'view_contact' => ' View contact',
    'message'=>'Message',
    'detail'=>'Detail',
    'not_seen'=>' Not Seen',
    'watched'=>'Watched',
    'sendmail'=>'Send mail',

    //categories
    'categories'=>'Categories',
    'add_cat'=>'Categories Create',
    'edit_cat'=>'Categories Update',
    'permalink'=>'Permalink',
    'description'=>'Description',
    'sort'=>'Sort',
    'translations'=>'Translations',
    'publish'=>'Publish',
    'save'=>'Save',
    'parent'=>'Parent',
    'name_vn'=>'Name Vietnamese',
    'name_jp'=>'Name Japanese',


    // News
    'news_list' => 'News List',
    'add' => 'Add News',
    'slug' => 'Slug',
    'avatar' => 'Avatar',
    'author' => 'Author',
    'delete_n'=>'Do you News want to delete this record ?',
    'add_new' => 'Add News',
    'url'=>'Slug Url',
    'vietnam' => 'Vietnam',
    'english'=>'English',
    'japan'=>'Japan',
    'title' => 'Title',
    'title_vn'=>'Title Vietnamese',
    'title_jp'=>'Title Japan',
    'title_en'=>'Title English',
    'content'=>'Content',
    'created_btn'=>'Create',
    'edit_news' => 'Edit News',
    'update'=>'Update',
    'choose_file'=>'Choose File',
    'file_not'=>'No file chosen...',
    // company
    'company_list'=>'Company List',
    'code'=>'Code',
    'logo'=>'Logo',
    'active'=>'Active',
    'choose'=>'Choose Default',
    'auto' => 'Auto Mail',
    'add_company'=>'Add Company',
    'mail_footer_en'=>'Footer Mail English',
    'mail_footer_jp' => 'Footer Mail Japan',
    'edit_company'=>'Edit Company',

    'company_c_list'=>'Company Contact List',
    'phone'=>'Phone',
    'fax'=>'Fax',
    'add_company_c'=>'Add Company Contact',
    'edit_company_c'=>'Edit Company Contact',
    'company_code' => 'Code Company',
    'hotline'=>'Hot line',
    'company_address'=>'Address Company',
    'company_select' => 'Select Code Company',
    'company_delete_n'=>"Cannot Remove Company",
    'company_delete_b'=>"Because Company Active Or Existed Company Contact",
    'close'=>'Close',
    // user
    'user_list'=>'User List',
    'username'=>'User Name',
    'roles'=>'Roles',
    'password'=>'Password',
    'c_password'=>'Confirm Password',
    'reset' => 'Reset',
    'add_user'=>'Add Users',
    'edit_user'=>'Edit Users',
    'edit_password'=>'Edit Password',
     'auto_mail'=>'Mail Auto',

    'log_list'=>'Activities Logs',
    'ip'=>'IPAddress',
    'history'=>'History',
    'more_log'=>'More',
    'log_time'=>'Log Time',

    'edit_sucss'=>'Edit Success',
    'add_sucss'=>'Add Success',
    'success'=>'Success!!',
    'role_log'=>'Cannot edit user because permission user !!',

    //Roles
    'roles_list'=>'Roles List',
    'roles_per'=>'Roles and Permissions',
    'add_roles'=>'Add Roles and Permissions ',
    'created_by'=>'Created By',
    'per_flags'=>'Permission Flags',
    'per_all'=>'All Permissions',
    'create-tasks' =>'Create Tasks',
    'edit-tasks' =>'Edit Tasks',
    'delete-tasks' =>'Delete Tasks',
    'create-users' =>'Create Users',
    'edit-users' =>'Edit User',
    'delete-users' =>'Delete Use',
    'password-users' =>'Reset Password User',
    'create-roles' =>'Create Roles',
    'edit-roles' =>'Edit Roles',
    'delete-roles' =>'Delete Roles',
    'view-logs' =>'Views Log System',




];

