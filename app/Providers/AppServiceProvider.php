<?php

namespace App\Providers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);

        view()->composer('admin.navbar',function ($view){
            $contacts = DB::table('contacts')
                ->where('status', 0)
                ->orderBy('created_at','desc')
                ->limit(10)
                ->get();
            $contacts_view = DB::table('contacts')->where('status', 0)->count();
            $view->with('contacts',$contacts)->with('contacts_view',$contacts_view);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
