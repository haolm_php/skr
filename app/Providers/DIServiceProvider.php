<?php

namespace App\Providers;

use App\ServiceImpl\AdminServiceImpl;
use App\ServiceImpl\CompanyServiceImpl;
use App\ServiceImpl\PostsServiceImpl;
use App\Services\AdminService;
use App\Services\CompanyService;
use App\Services\PostsService;
use Illuminate\Support\ServiceProvider;

class DIServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AdminService::class, function () {
            return new AdminServiceImpl();
        });
        $this->app->singleton(CompanyService::class, function () {
            return new CompanyServiceImpl();
        });
        $this->app->singleton(PostsService::class, function () {
            return new PostsServiceImpl();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}
