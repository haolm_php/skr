<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table ="news";
    protected $fillable = [
        'tittle_vn','tittle_jp','tittle_en', 'avatar', 'content_vn','content_jp','content_en','slug','author','created_at','updated_at'
    ];
    public $timestamps= true;

}
