<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ThanksYouMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var $request
     */
    public $request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->content = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = DB::table('company')
            ->where('company.active', 1)
            ->get();
        foreach ($company as $row){
            $company_contact = DB::table('company')
                ->leftJoin('company_contact', 'company.code', '=', 'company_contact.code_company')
                ->where('company.active', 1)
                ->get();
            $stt=1;

            $logo=asset('/img/upload/'.$row->logo);
            $base_url= url('/');
            return $this->subject('Mail Thanks You')
                ->view('mails.thank_you_mail')
                ->with([
                    'content' => $this->content,
                    'company'=>$row,
                    'logo'=>$logo,
                    'url_base'=>$base_url,
                    'company_contact'=>$company_contact,
                    'stt'=>$stt
                ]);
        }
    }
}
