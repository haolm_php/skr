<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\PendingMail;
use App\Mail\ThanksYouMail;


class SendThanksMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $cc_mail;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data,$cc_mail)
    {
            $this->data = $data;
            $this->cc_mail = $cc_mail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->data['email'])
            ->cc($this->cc_mail)
            ->send(new ThanksYouMail($this->data));
     }
}
