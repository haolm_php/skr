<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDescription extends Model
{
    protected $table ="category_description";
    protected $fillable = [
        'categories_id', 'name','lang','description','created_at','updated_at'
    ];
    public $timestamps= true;
    public function category(){
        return $this->belongsTo('App\CategoryModel');
    }
    //
}
