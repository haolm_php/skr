<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    protected $table ="users_permissions";
    protected $fillable = [
        'user_id','permission_id'
    ];
}
