<?php

namespace App\Services;
use Illuminate\Http\Request;

interface CompanyService
{

    /**
     * @param $request Request
     * @return string
     */
    function validateCompany($request);
    function validateEditCompany($request);
    function addCompany($request);
    function editCompanyById($id);
    function editCompany($request);
    function activeCompany($request);
    function deleteCompany($request);
    function companyAutoMail($request);
    /**
     * @param $request Request
     * @return string
     */
    function companyCode();
    function validateCompanyCt($request);
    function addCompanyCt($request);
    function editCompanyCtById($id);
    function editCompanyCt($request);
    function deleteCompanyCt($request);

}
