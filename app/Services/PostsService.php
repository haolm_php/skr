<?php

namespace App\Services;
use Illuminate\Http\Request;

interface PostsService
{
    //Categories
    function validateAddCategories($request);
    function addCategory($request);
    function parentCategory();
    function getEditCategories($request);
    function editCategories($request);
    function deleteCategories($request);

    /**
     * @param $request Request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function validateAddParams($request);
    function validateEditParams($request);
    /**
     * @param $request Request
     * @return string
     */
    function editNewById($id);
    function getNew();
    function addNew($request);
    function editNew($id);
    function deleteNew($request);
    function hotNews($request);
    function newsHot();
}
