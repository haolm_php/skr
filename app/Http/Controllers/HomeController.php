<?php

namespace App\Http\Controllers;

use App\Jobs\SendThanksMail;
use App\Mail\ThanksYouMail;
use App\NewModel;
use App\Services\AdminService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Lang;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{

    protected $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        return view('mails.thank_you_mail');
    }

    public function index()
    {
        $new = DB::table('news')
            ->where('news.del_flg', '0')
            ->orderBy('created_at', 'ASC')->get();

        return view('home', [
            'new' => $new
        ]);
    }

    public function principles()
    {
        return view('principles');
    }

    public function execution()
    {
        return view('execution');
    }

    function businessOutline()
    {
        return view('business_outline');
    }

    public function newDetail($slug)
    {
        $news = NewModel::where('slug', $slug)->first();
        $news_hot = $this->adminService->newsHot();
        return view('new_detail', [
            'news' => $news,
            'news_hot' => $news_hot
        ]);

    }

    public function postContact (Request $request)
    {
        //chuyển thời gian về giây.
        $time = 30;


        $validator = $this->adminService->validateContact($request);
        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->adminService->addContact($request);
            $company = DB::table('company')
                ->where('company.active', 1)
                ->where('company.auto_relay', 1)
                ->count();
            $admin_email = $this->adminService->emailAdmin();
            if ($company > 0){
                if ($admin_email != null) {
                    dispatch(new SendThanksMail($request->all(),$admin_email))
                        ->delay(now()->addMinutes(0.1));
//                Mail::to($request->email)
//                    ->cc($admin_email)
//                    ->send(new ThanksYouMail($request));

                } else {
                    Mail::to($request->email)
                        ->send(new ThanksYouMail($request));
                }
            }
            alert()->success(Lang::get('messages.success'), Lang::get('messages.success_mail'));
            return redirect()->back();
        }
    }
}

