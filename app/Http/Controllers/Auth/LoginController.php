<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }
    public function username()
    {
        // return 'email';
        return 'username';

    }
    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {
        Activity::saving(function (Activity $activity) {
            $activity->properties = $activity->properties->put('ip', request()->ip());
            $activity->properties = $activity->properties->put('browser', request()->header('User-Agent'));
        });
        activity()
            ->causedBy($user)
            ->performedOn($user)
            ->log('Login');
    }
    public function logout(Request $request)
    {
        Activity::saving(function (Activity $activity) {
            $activity->properties = $activity->properties->put("ip", request()->ip());
            $activity->properties = $activity->properties->put("browser", request()->header('User-Agent'));
        });
        activity()
            ->causedBy(Auth::user())
            ->log('Log Out ');
        $this->guard()->logout();
        return redirect()->route('login');
    }
}
