<?php

namespace App\Http\Controllers\Admin;
use App\Services\CompanyService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CompanyController extends Controller
{
    protected $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->middleware('auth');
        $this->companyService = $companyService;
    }
    // Company  -------------------------------------------------------------------------------------------------
    public function getCompany()
    {
        return view('admin/company.index');
    }

    public function getAddCompany(Request $request)
    {
        if ($request->user()->can('create-tasks')) {
            return view('admin/company.add');
        }
        return redirect('admin/company')->with('status', trans('admin/layout.role_log'));

    }

    public function postAddCompany(Request $request)
    {
        $validator = $this->companyService->validateCompany($request);
        if ($validator->fails()) {
            return redirect('admin/company-add')->withErrors($validator)->withInput();
        } else {
            $this->companyService->addCompany($request);
            return redirect('admin/company')->with('status', trans('admin/layout.add_sucss'));
        }
    }

    // Edit New
    public function getEditCompany(Request $request,$id)
    {
        if ($request->user()->can('edit-tasks')) {
            $company = $this->companyService->editCompanyById($id);
            return view('admin/company.edit', [
                'company' => $company
            ]);
        }
        return redirect('admin/company')->with('status', trans('admin/layout.role_log'));

    }

    public function postEditCompany(Request $request)
    {
        $id = $request->id;
        $validator = $this->companyService->validateEditCompany($request);
        if ($validator->fails()) {
            return redirect('admin/company-edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->companyService->editCompany($request);
            return redirect('admin/company')->with('status', trans('admin/layout.edit_sucss'));
        }
    }

    function postActiveCompany()
    {
        $request = request();
        $this->companyService->activeCompany($request);
        return 1;
    }

    public function postDeleteCompany()
    {
        $request = request();
        if ($request->user()->can('delete-tasks')) {
            $delete = $this->companyService->deleteCompany($request);
            return $delete;
        }
        return 1;
    }

    public function companyAutoMail(Request $request){
        $this->companyService->companyAutoMail($request);
        return 1;
    }

    // Company Contact -------------------------------------------------------------------------------------------------
    public function getCompanyCt()
    {
        return view('admin/company/company_contact.index');
    }

    public function getAddCompanyCt(Request $request)
    {
        if ($request->user()->can('delete-tasks')) {
            $code = $this->companyService->companyCode();
            return view('admin/company/company_contact.add')->with('code', $code);
        }
        return redirect('admin/company-contact')->with('status', trans('admin/layout.add_sucss'));
    }

    public function postAddCompanyCt(Request $request)
    {
        $validator = $this->companyService->validateCompanyCt($request);
        if ($validator->fails()) {
            return redirect('admin/company-contact-add')->withErrors($validator)->withInput();
        } else {
            $this->companyService->addCompanyCt($request);
            return redirect('admin/company-contact')->with('status',trans('admin/layout.add_sucss'));
        }
    }

    // Edit getEditCompanyCt
    public function getEditCompanyCt(Request $request,$id)
    {
        if ($request->user()->can('edit-tasks')) {
            $code = $this->companyService->companyCode();
            $company = $this->companyService->editCompanyCtById($id);
            return view('admin/company/company_contact.edit', [
                'company' => $company,
                'code' => $code
            ]);
        }
        return redirect('admin/company-contact')->with('status', trans('admin/layout.role_log'));
    }

    public function postEditCompanyCt(Request $request)
    {
        $id = $request->id;
        $validator = $this->companyService->validateCompanyCt($request);
        if ($validator->fails()) {
            return redirect('admin/company-contact-edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->companyService->editCompanyCt($request);
            return redirect('admin/company-contact')->with('status',trans('admin/layout.edit_sucss'));
        }
    }

    public function postDeleteCompanyCt()
    {
        $request = request();
        if ($request->user()->can('delete-tasks')) {
            $this->companyService->deleteCompanyCt($request);
        }
        return 0;
    }

    public function Activity(Request $request){
        if ($request->user()->can('view-logs')) {
            return view('admin/log.index');
        }
        return redirect()->back()->with('status', trans('admin/layout.role_log'));

    }
}
