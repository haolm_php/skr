<?php

namespace App\Http\Controllers\Admin;

use App\ServiceImpl\AdminServiceImpl;
use App\ServiceImpl\CompanyServiceImpl;
use App\ServiceImpl\PostsServiceImpl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiAdminController extends Controller
{
    private $adminServiceImpl;
    private $companyServiceIml;
    private $postsServiceImpl;

    public function __construct(AdminServiceImpl $adminServiceImpl
        ,CompanyServiceImpl $companyServiceIml
        ,PostsServiceImpl $postsServiceImpl)
    {
        $this->adminServiceImpl = $adminServiceImpl;
        $this->companyServiceIml = $companyServiceIml;
        $this->postsServiceImpl = $postsServiceImpl;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return $this->adminServiceImpl->getContact();
    }
    public function contactRelay()
    {
        return $this->adminServiceImpl->getContactRelay();
    }
    public function getUser()
    {
        return $this->adminServiceImpl->getUser();
    }
    public function getRoles(){
        return $this->adminServiceImpl->getRoles();
    }
    public function getNew()
    {
        return $this->postsServiceImpl->getNew();
    }
    public function chartNew(){
        return $this->adminServiceImpl->chartNew();
    }
    public function getCompany(){
        return $this->companyServiceIml->getCompany();
    }
    public function getCompanyCt(){
        return $this->companyServiceIml->getCompanyCt();
    }
    public function getLog(){
        return $this->adminServiceImpl->getLog();
    }

    public function getCategories(){
        return $this->postsServiceImpl->getCategories();
    }


}
