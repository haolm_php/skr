<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



//Rate list
Route::get('admin/api/contact', 'Admin\ApiAdminController@contact')->name('api.contact.index');
Route::get('admin/api/contact-relay', 'Admin\ApiAdminController@contactRelay')->name('api.contact_relay.index');

//User list
Route::get('admin/api/user', 'Admin\ApiAdminController@getUser')->name('api.user.index');
//Roles List
Route::get('admin/api/roles', 'Admin\ApiAdminController@getRoles')->name('api.role.index');
//New list
Route::get('admin/api/new', 'Admin\ApiAdminController@getNew')->name('api.new.index');
//New information
Route::get('admin/api/chart-new', 'Admin\ApiAdminController@chartNew')->name('api.chart.new');

//Company list
Route::get('admin/api/company', 'Admin\ApiAdminController@getCompany')->name('api.company.index');
//Company Contact list
Route::get('admin/api/company-contact', 'Admin\ApiAdminController@getCompanyCt')->name('api.company_ct.index');
//Activity Log list
Route::get('admin/api/activity-log', 'Admin\ApiAdminController@getLog')->name('api.log.index');

Route::get('admin/api/categories', 'Admin\ApiAdminController@getCategories')->name('api.categories.index');