<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'as'=>'home',
    'uses'=>'HomeController@index'
]);

Auth::routes();
Auth::routes(['register' => false]);
Route::get('/test', 'HomeController@test')->name('test');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/principles', 'HomeController@principles')->name('principles');
Route::get('/execution-order', 'HomeController@execution')->name('execution');
Route::get('/business-outline', 'HomeController@businessOutline')->name('businessOutline');
Route::get('news/{slug}', 'HomeController@newDetail')->name('new_detail');

Route::get('lang/{lang}','LangController@lang')->name('lang');
Route::post('/contact/data',[
    'as'=>'contact_post',
    'uses'=>'HomeController@postContact']);

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {

    Route::get('admin/{lang}','LangController@Adminlang')->name('lang_admin');
    Route::get('/', 'Admin\AdminController@index')->name('admin');
    // Category
    Route::get('/categories',[
        'as'=>'categories',
        'uses'=>'Admin\PostsController@categories']);
    Route::get('/categories-add',[
        'as'=>'cat_add',
        'uses'=>'Admin\PostsController@getAddCategory']);
    Route::post('/categories-add-post',[
        'as'=>'post_category_add',
        'uses'=>'Admin\PostsController@postAddCategory']);
    Route::get('/categories-edit/{id}',[
        'as'=>'cat_edit',
        'uses'=>'Admin\PostsController@getEditCategory']);
    Route::post('/categories-edit-post/{id}',[
        'as'=>'post_category_edit',
        'uses'=>'Admin\PostsController@postEditCategory']);
    Route::post('/categories-delete',[
        'as'=>'categories_delete',
        'uses'=>'Admin\PostsController@postDeleteCategory']);
    // New
    Route::get('/news',[
        'as'=>'new',
        'uses'=>'Admin\PostsController@new']);
    Route::get('/new-add',[
        'as'=>'new_add',
        'uses'=>'Admin\PostsController@getAddNew']);
    Route::post('/new-add-post',[
        'as'=>'new_add_post',
        'uses'=>'Admin\PostsController@postAddNew']);

    Route::get('/new-edit/{id}',[
        'as'=>'new_edit',
        'uses'=>'Admin\PostsController@getEditNew']);
    Route::post('/new-edit-post/{id}',[
        'as'=>'new_edit_post',
        'uses'=>'Admin\PostsController@postEditNew']);

    Route::post('/new-delete',[
        'as'=>'new_delete',
        'uses'=>'Admin\PostsController@postDeleteNew']);
    Route::post('/news-hot',[
        'as'=>'news_hot',
        'uses'=>'Admin\PostsController@postNewsHot']);

    // User
    Route::get('/user',[
        'as'=>'user',
        'uses'=>'Admin\AdminController@user']);
    Route::get('/user-add',[
        'as'=>'user_add',
        'uses'=>'Admin\AdminController@getAddUser']);
    Route::post('/user-add-post',[
        'as'=>'user_add_post',
        'uses'=>'Admin\AdminController@postAddUser']);
    Route::get('/user-edit/{id}',[
        'as'=>'user_edit',
        'uses'=>'Admin\AdminController@getEditUser']);
    Route::post('/user-edit-post/{id}',[
        'as'=>'user_edit_post',
        'uses'=>'Admin\AdminController@postEditUser']);
    Route::get('/user-edit-password/{id}',[
        'as'=>'user_edit',
        'uses'=>'Admin\AdminController@getEditUserPass']);
    Route::post('/user-edit-post-password/{id}',[
        'as'=>'user_edit_pass_post',
        'uses'=>'Admin\AdminController@postEditUserPass']);

    Route::post('/user-delete',[
        'as'=>'user_delete',
        'uses'=>'Admin\AdminController@postDeleteUser']);
    Route::post('/user-role',[
        'as'=>'user_role',
        'uses'=>'Admin\AdminController@roleUser'
    ]);
    //Dashboard
    Route::get('/dashboard',[
        'as'=>'dashboard',
        'uses'=>'Admin\AdminController@index']);

    //Contact
    Route::get('/contact',[
        'as'=>'get_post',
        'uses'=>'Admin\AdminController@getContact']);
    Route::post('/contact-delete',[
        'as'=>'contact_delete',
        'uses'=>'Admin\AdminController@deleteContact']);

    Route::get('/contact/detail/{id}',[
        'as'=>'contact_detail',
        'uses'=>'Admin\AdminController@detailContact']);

    Route::post('/contact/relay-mail',[
        'as'=>'relay_mail',
        'uses'=>'Admin\AdminController@relayMail']);

    // Company
    Route::get('/company',[
        'as'=>'company',
        'uses'=>'Admin\CompanyController@getCompany']);
    Route::get('/company-add',[
        'as'=>'company_add',
        'uses'=>'Admin\CompanyController@getAddCompany']);
    Route::post('/post-company-add',[
        'as'=>'post_company_add',
        'uses'=>'Admin\CompanyController@postAddCompany']);
    Route::get('/company-edit/{id}',[
        'as'=>'company_edit',
        'uses'=>'Admin\CompanyController@getEditCompany']);
    Route::post('/post-company-edit/{id}',[
        'as'=>'post_company_edit',
        'uses'=>'Admin\CompanyController@postEditCompany']);
    Route::post('/post-company-delete',[
        'as'=>'company_delete',
        'uses'=>'Admin\CompanyController@postDeleteCompany']);
    Route::post('/post-company-active',[
        'as'=>'company_active',
        'uses'=>'Admin\CompanyController@postActiveCompany']);
    Route::post('/company-auto-mail',[
        'as'=>'company_auto',
        'uses'=>'Admin\CompanyController@companyAutoMail']);

    // Company Contact
    Route::get('/company-contact',[
        'as'=>'company',
        'uses'=>'Admin\CompanyController@getCompanyCt']);
    Route::get('/company-contact-add',[
        'as'=>'company_add',
        'uses'=>'Admin\CompanyController@getAddCompanyCt']);
    Route::post('/post-company-contact-add',[
        'as'=>'post_company_ct_add',
        'uses'=>'Admin\CompanyController@postAddCompanyCt']);
    Route::get('/company-contact-edit/{id}',[
        'as'=>'company_ct_edit',
        'uses'=>'Admin\CompanyController@getEditCompanyCt']);
    Route::post('/post-company-contact-edit/{id}',[
        'as'=>'post_company_ct_edit',
        'uses'=>'Admin\CompanyController@postEditCompanyCt']);
    Route::post('/post-company-contact-delete',[
        'as'=>'company_ct_delete',
        'uses'=>'Admin\CompanyController@postDeleteCompanyCt']);
    // Activity Log
    Route::get('/activity', 'Admin\CompanyController@Activity')->name('log');
    //Roles
    Route::get('/roles',[
        'as'=>'roles',
        'uses'=>'Admin\AdminController@getRoles']);
    Route::get('/add-roles',[
        'as'=>'roles',
        'uses'=>'Admin\AdminController@getAddRoles']);
    Route::post('/post-roles-add',[
        'as'=>'roles_add_post',
        'uses'=>'Admin\AdminController@postAddRoles']);
    Route::get('/edit-roles/{id}',[
        'as'=>'edit-roles',
        'uses'=>'Admin\AdminController@geteditRoles']);
    Route::post('/post-roles-edit/{id}',[
        'as'=>'roles_edit_post',
        'uses'=>'Admin\AdminController@postEditRoles']);
    Route::post('/post-roles-delete',[
        'as'=>'roles_delete',
        'uses'=>'Admin\AdminController@deleteRoles']);
});